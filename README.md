

### Description
InfluxDB is a schemaless database. You can add new measurements, tags, and fields at any time. Note that if you attempt to write data with a different type than previously used (for example, writing a string to a field that previously accepted integers), InfluxDB will reject those data.

#### Terminology
The table below is a (very) simple example of a table called foodships in an SQL database with the unindexed column #_foodships and the indexed columns park_id, planet, and time.

        +---------+---------+---------------------+--------------+
        | park_id | planet  | time                | #_foodships  |
        +---------+---------+---------------------+--------------+
        |       1 | Earth   | 1429185600000000000 |            0 |
        |       1 | Earth   | 1429185601000000000 |            3 |
        |       2 | Saturn  | 1429185602000000000 |           10 |
        |       4 | Saturn  | 1429185602000000000 |            6 |
        |       4 | Saturn  | 1429185603000000000 |            5 |
        +---------+---------+---------------------+--------------+

An InfluxDB measurement (foodships) is similar to an SQL database table.
InfluxDB tags ( park_id and planet) are like indexed columns in an SQL database.
InfluxDB fields (#_foodships) are like unindexed columns in an SQL database.
InfluxDB points (for example, 2015-04-16T12:00:00Z 5) are similar to SQL rows.

Example insert to the table:

    INSERT foodships,park_id=1,planet=Earth #_foodships=0 1429185600000000000


InfluxDB is a time series database. Optimizing for this use case entails some tradeoffs, primarily to increase performance at the cost of functionality. Below is a list of some of those design insights that lead to tradeoffs:

For the time series use case, we assume that if the same data is sent multiple times, it is the exact same data that a client just sent several times.

Pro: Simplified conflict resolution increases write performance.
Con: Cannot store duplicate data; may overwrite data in rare circumstances.

Deletes are a rare occurrence. When they do occur it is almost always against large ranges of old data that are cold for writes.

Pro: Restricting access to deletes allows for increased query and write performance.
Con: Delete functionality is significantly restricted.

Updates to existing data are a rare occurrence and contentious updates never happen. Time series data is predominantly new data that is never updated.

Pro: Restricting access to updates allows for increased query and write performance.
Con: Update functionality is significantly restricted.

The vast majority of writes are for data with very recent timestamps and the data is added in time ascending order.

Pro: Adding data in time ascending order is significantly more performant.
Con: Writing points with random times or with time not in ascending order is significantly less performant.

Scale is critical. The database must be able to handle a high volume of reads and writes.

Pro: The database can handle a high volume of reads and writes.
Con: The InfluxDB development team was forced to make tradeoffs to increase performance.

Being able to write and query the data is more important than having a strongly consistent view.

Pro: Writing and querying the database can be done by multiple clients and at high loads.
Con: Query returns may not include the most recent points if database is under heavy load.

Many time series are ephemeral. There are often time series that appear only for a few hours and then go away, e.g. a new host that gets started and reports for a while and then gets shut down.

Pro: InfluxDB is good at managing discontinuous data.
Con: Schema-less design means that some database functions are not supported e.g. there are no cross table joins.

No one point is too important.

Pro: InfluxDB has very powerful tools to deal with aggregate data and large data sets.
Con: Points don’t have IDs in the traditional sense, they are differentiated by timestamp and series.

### Units in influx

    +---------+---------+
    | Letter | planet    |
    +---------+---------+-
    |    ns	|nanoseconds |
    |    u o|microseconds|
    |    ms	|milliseconds|
    |    s	|seconds     |
    |    m	|minutes     |
    |    h	|hours       |
    |    d	|days        |
    |    w	|weeks       |
    +---------+---------+

##### Http status codes:
    
    2xx: If your write request received HTTP 204 No Content, it was a success!
    4xx: InfluxDB could not understand the request.
    5xx: The system is overloaded or significantly impaired.


### Setup

    docker pull influxdb
    docker run --rm influxdb influxd config > influxdb.conf
    docker run -p 8086:8086 infludb
    OR
    docker run -p 8086:8086 -v $PWD/influxdb.conf:/etc/influxdb/influxdb.conf:ro influxdb -config /etc/influxdb/influxdb.conf
    
 
## Python

    pip3 install influxdb
   
    
### Client 
    sudo apt install influxdb-client
run:
 influx -precision rfc3339
    
### Create DB
influxdb cli

    CREATE DATABASE mydb
    or    
    curl -i -XPOST http://localhost:8086/query --data-urlencode "q=CREATE DATABASE mydb"



### Show Database
    SHOW DATABASES

### Use exact database
influxdb cli
    
    USE mydb

### Insert
    curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'
    or
    INSERT cpu,host=serverA,region=us_west value=0.64
    
insert batch:
        
        curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary 'cpu_load_short,host=server02 value=0.67
                    cpu_load_short,host=server02,region=us-west value=0.55 1422568543702900257
                    cpu_load_short,direction=in,host=server01,region=us-west value=2.0 1422568543702900257'


the file with data 
cpu_data.txt
        
        cpu_load_short,host=server02 value=0.67
        cpu_load_short,host=server02,region=us-west value=0.55 1422568543702900257
        cpu_load_short,direction=in,host=server01,region=us-west value=2.0 1422568543702900257

batch insert: 
    
    curl -i -XPOST "http://localhost:8086/write?db=mydb" --data-binary 'mymeas,mytag=3 myfield=89 1463689152000000000
    mymeas,mytag=2 myfield=34 1463689152000000000'

 
### Select data
    SELECT "host", "region", "value" FROM "cpu"
    SELECT * FROM events, errors
    SELECT * FROM "temperature"
    SELECT * FROM /.*/ LIMIT 1  # select fist line from all tables 
    SELECT * FROM "cpu_load_short" WHERE "value" > 0.9
    SELECT mean("temp") FROM /\.north$/ # Schema 1 - Query for data encoded in the measurement name
    SELECT mean("temp") FROM "weather_sensor" WHERE "region" = 'north' #  Schema 2 - Query for data encoded in tags
### Start docker Client
    docker exec -it influxdb influx
    
### Like show tables with columns 

    SHOW SERIES ON mydb
    or
    SHOW SERIES     
    SHOW SERIES ON NOAA_water_database WHERE time < now() - 28d
    SHOW SERIES ON NOAA_water_database WHERE time < now() - 1m

### Delete all series    
    DROP SERIES FROM /.*/

### Like show tables
    SHOW MEASUREMENTS
    SHOW MEASUREMENTS ON mydb
    
### Show tags like primary key

    SHOW TAG VALUES [ON <database_name>][FROM_clause] WITH KEY [ [<operator> "<tag_key>" | <regular_expression>] | [IN ("<tag_key1>","<tag_key2")]] [WHERE <tag_key> <operator> ['<tag_value>' | <regular_expression>]] [LIMIT_clause] [OFFSET_clause]
    
### Show field key

    SHOW FIELD KEYS [ON <database_name>] [FROM <measurement_name>]
    
    
### Show field is like show columns
    SHOW FIELD KEYS ON "NOAA_water_database"
    
    
## Filter/group  test
https://docs.influxdata.com/influxdb/v1.7/query_language/schema_exploration/#filter-meta-queries-by-time\
Example filtering SHOW TAG KEYS by time

    Specify a shard duration on a new database or alter an existing shard duration. To specify a 1h shard duration when creating a new database, run the following command:

    > CREATE database mydb with duration 7d REPLICATION 1 SHARD DURATION 1h name myRP;

        Note: The minimum shard duration is 1h.

    Verify the shard duration has the correct time interval (precision) by running the SHOW SHARDS command. The example below shows a shard duration with an hour precision.

    > SHOW SHARDS
    name: mydb
    id database retention_policy shard_group start_time end_time expiry_time owners
    -- -------- ---------------- ----------- ---------- -------- ----------- ------
    > precision h

    (Optional) Insert sample tag keys. This step is for demonstration purposes. If you already have tag keys (or other meta data) to search for, skip this step.

    // Insert a sample tag called "test_key" into the "test" measurement, and then check the timestamp:
    > INSERT test,test_key=hello value=1

    > select * from test
    name: test
    time test_key value
    ---- -------- -----
    434820 hello 1

    // Add new tag keys with timestamps one, two, and three hours earlier:

    > INSERT test,test_key_1=hello value=1 434819
    > INSERT test,test_key_2=hello value=1 434819
    > INSERT test,test_key_3_=hello value=1 434818
    > INSERT test,test_key_4=hello value=1 434817
    > INSERT test,test_key_5_=hello value=1 434817

    To find tag keys within a shard duration, run one of the following commands:

    SHOW TAG KEYS ON database-name <WHERE time clause> OR

    SELECT * FROM measurement <WHERE time clause>

    The examples below use test data from step 3.

    //Using data from Step 3, show tag keys between now and an hour ago
    > SHOW TAG KEYS ON mydb where time > now() -1h and time < now()
    name: test
    tagKey
    ------
    test_key
    test_key_1
    test_key_2

    // Find tag keys between one and two hours ago
    > SHOW TAG KEYS ON mydb where > time > now() -2h and time < now()-1h
    name: test
    tagKey
    ------
    test_key_1
    test_key_2
    test_key_3

    // Find tag keys between two and three hours ago
    > SHOW TAG KEYS ON mydb where > time > now() -3h and time < now()-2h
    name: test
    tagKey
    ------
    test_key_3
    test_key_4
    test_key_5

    // For a specified measurement, find tag keys in a given shard by specifying the time boundaries of the shard
    > SELECT * FROM test WHERE time >= '2019-08-09T00:00:00Z' and time < '2019-08-09T10:00:00Z'
    name: test
    time test_key_4 test_key_5 value
    ---- ------------ ------------ -----
    434817 hello 1
    434817 hello 1

    // For a specified database, find tag keys in a given shard by specifying the time boundaries of the shard
    > SHOW TAG KEYS ON mydb WHERE time >= '2019-08-09T00:00:00Z' and time < '2019-08-09T10:00:00Z'
    name: test
    tagKey
    ------
    test_key_4
    test_key_5

### Continuous Queries

Continuous queries (CQ) are InfluxQL queries that run automatically and periodically on realtime data and store query results in a specified measurement.
    
    CREATE CONTINUOUS QUERY <cq_name> ON <database_name>
    BEGIN
        
        SELECT * INTO [<retention policy>.]<measurement> [ON <database>] FROM <measurement> [WHERE ...] [GROUP BY ...]
    END
    
    
#### Shard group duration management
InfluxDB stores data in shard groups. Shard groups are organized by retention policy (RP) and store data with timestamps that fall within a specific time interval called the shard duration.

If no shard group duration is provided, the shard group duration is determined by the RP’s duration at the time the RP is created. The default values are:


## Shard
Shards are ideal containers for time series data. Sharding the data within InfluxDB allows for a highly scalable approach for boosting throughput and overall performance, especially considering that the data in a Time Series Database will in all likelihood grow over time.
Shards contain temporal blocks of data and are mapped to an underlying storage engine database. The InfluxDB storage engine is called TSM or Time-Structured Merge Tree and is remarkably similar to an LSM Tree.

    SHOW shard groups
